#!/bin/bash

docker stop express1

# Delete the old repo
sudo rm -rf /home/ec2-user/express1

# any future command that fails will exit the script
set -e

# BE SURE TO UPDATE THE FOLLOWING LINE WITH THE URL FOR YOUR REPO
git clone https://gitlab.com/trisitiannn/express1.git

cd /home/ec2-user/express1

# run the node app in a container
deploy/express1.sh -d
